// AudioStreaming.cpp : Define el punto de entrada de la aplicación.
//
//#include <vld.h>
#include "AudioStreaming.h"

void data_callback (ma_device * pDevice, void *pOutput, const void *pInput, ma_uint32 frameCount)
{

    if ( meter && !IsIconic (g_wnd) )
        meter->AppendSamples (( short * )pInput, frameCount);

    if ( isStreaming && networkQueue.size () < 100 )
    {
        networkQueue.push_back (std::string (&(( char * )pInput)[0], &(( char * )pInput)[frameCount * 4]));
    }

    ( void )pOutput;
}

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPWSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    //AllocConsole ();
    //freopen ("CONOUT$", "w", stdout);
    
    LoadStringW(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadStringW(hInstance, IDC_AUDIOSTREAMING, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);

    meter = new CdBMeter (2);
    config = new CPreferences ();
    mp3 = new CMp3Encoder ();
    ProcTempFileSave ();
    audio = new CAudio ();

    if ( !config->ReadDWORD (L"default_device", &defaultDevice) )   defaultDevice = 0;
    if ( !config->ReadDWORD (L"is_streaming", &isStreaming))        isStreaming = FALSE;
    if ( !config->ReadDWORD (L"settings_panel", &settingsPanel) )   settingsPanel = TRUE;
    if ( !config->ReadDWORD (L"save_mp3", &saveMP3) )               saveMP3 = FALSE;
    if ( !config->ReadStringW (L"mp3_save_path", mp3path) )         mp3path[0] = '\0';
    if ( !config->ReadStringW (L"server_url", serverURL) )          serverURL[0] = '\0';
    if ( !config->ReadStringW (L"server_key", serverKEY) )          serverKEY[0] = '\0';

    // Realizar la inicialización de la aplicación:
    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    fillDeviceListBox (audio->QueryDevices (), defaultDevice);
    audio->CaptureByID (defaultDevice);

    HANDLE thread_net = CreateThread (NULL, NULL, NetworkProcessor, 0, NULL, NULL);
    
    d3d_gui = new CD3DGUI (lbl_d3d);

    while ( !quit )
    {

        if ( !IsIconic (g_wnd) )
        {
            d3d_gui->Render ();
        }

        MSG msg;
        while ( PeekMessage (&msg, NULL, 0, 0, PM_REMOVE) )
        {
            if ( msg.message == WM_QUIT )
                quit = true;
            TranslateMessage (&msg);
            DispatchMessage (&msg);
        }
        
        Sleep (1000 / 30);

    }

    quit = true;
    SAFE_RELEASE (d3d_gui);
    isStreaming = FALSE;
    SAFE_DELE (audio);
    SAFE_DELE (meter);
    TerminateThread (thread_net, 0);
    saveMP3 = false;
    ProcTempFileSave ();
    SAFE_DELE (mp3);
    SAFE_DELE (config);
    return 0;
}

ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEXW wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_AUDIOSTREAMING));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = CreateSolidBrush (RGB (255, 255, 255));
    wcex.lpszMenuName   = 0;
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

    return RegisterClassExW(&wcex);
}

BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance; // Almacenar identificador de instancia en una variable global
   int vwin = 320;
   if ( !settingsPanel )
       vwin = 190;

   HWND hWnd = CreateWindowW(szWindowClass, szTitle, WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX , 200, 200, 495, vwin, nullptr, nullptr, hInstance, nullptr);
   if (!hWnd)
   {
      return FALSE;
   }

   g_wnd = hWnd;

   ShowWindow (hWnd, nCmdShow);
   UpdateWindow (hWnd);

   CreateWinApiGUI (hWnd, hInstance);
   
   return TRUE;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    //brush for outside background
    static HBRUSH hBrush = CreateSolidBrush (RGB (255, 255, 255));
    static HBRUSH hbrGray = CreateSolidBrush (RGB (222, 222, 222));;

    switch (message)
    {
   
    case WM_ERASEBKGND:
    {
        HDC hdc = ( HDC )wParam;
        RECT rc;  rc.top = 0; rc.left = 0; rc.right = 500; rc.bottom = 150;
        FillRect (hdc, &rc, hbrGray);
        rc.top = 150; rc.left = 0; rc.right = 500; rc.bottom = 500;
        FillRect (hdc, &rc, hBrush);
        return 1L;
    } break;
    case WM_TIMER:
        {
            //proc file timer
            if ( wParam == 1 )
            {
                ProcTempFileSave ();
                if ( isStreaming )
                {
                    static wchar_t text[100];
                    swprintf (text, L"Streaming at %.2f Kb/s", ( float )( ( float )transmittedBytes / 125.0f  ));
                    transmittedBytes = 0;
                    SetWindowText (lbl_networkstatus, text);
                }
            }
        }
        break;
    //case WM_CTLCOLOREDIT:
    //case WM_CTLCOLORLISTBOX:
    case WM_CTLCOLORSTATIC:
    //case WM_CTLCOLORBTN:
        {
            //set components color and background
            {
                HDC hdcStatic = ( HDC )wParam;
                SetTextColor (hdcStatic, RGB (0, 0, 0));
                SetBkColor (hdcStatic, RGB (255, 255, 255));
                if ( ( HWND )lParam == lbl_networkstatus )
                {
                    SetBkColor (hdcStatic, RGB (222, 222, 222));
                    return ( INT_PTR )hbrGray;
                }
                return ( INT_PTR )hBrush;
            }
        }
    case WM_COMMAND:
        {
            //printf ("hwnd %d  msg %d  wparam %d lparam %d\n", hWnd, message, wParam, lParam);
            //device changed
            if ( HIWORD (wParam) == CBN_SELCHANGE )
            {
                int ItemIndex = SendMessage (( HWND )lParam, ( UINT )CB_GETCURSEL,
                                             ( WPARAM )0, ( LPARAM )0);
                config->WriteDWORD (L"default_device", ItemIndex);
                
                audio->CaptureByID (ItemIndex);
            }

            //button clicked
            if ( message == 273 && wParam == 0)
            {
                if ( (HWND)lParam == hwnd_buttonsettings )
                {
                    settingsPanel ^= 1;
                    config->WriteDWORD (L"settings_panel", settingsPanel);
                    
                    RECT rc;
                    GetWindowRect (hWnd, &rc);
                    MoveWindow (hWnd, rc.left, rc.top, 495, settingsPanel==1?320:190, TRUE);
                }

                if ( ( HWND )lParam == hwnd_buttonstreaming )
                {
                    isStreaming ^= 1;
                    config->WriteDWORD (L"is_streaming", isStreaming);
                    SetWindowText (hwnd_buttonstreaming, isStreaming ? L"STREAMING" : L"NOT STREAMING");
                }

                if ( ( HWND )lParam == hwnd_mp3pathselect )
                {
                    CreateThread (NULL, NULL, PathSelect, NULL, NULL, NULL);
                }

                if ( ( HWND )lParam == hwnd_mp3savestatus )
                {
                    if ( !mp3path && saveMP3 == FALSE ) return 0;
                    if ( !PathFileExists (mp3path) && saveMP3 == FALSE ) return 0;
                    saveMP3 ^= 1;
                    config->WriteDWORD (L"save_mp3", saveMP3);
                    SetWindowText (hwnd_mp3savestatus, saveMP3 ? L"RECORDING" : L"NOT RECORDING");
                    EnableWindow  (hwnd_mp3pathselect, saveMP3 ? FALSE : TRUE);
                    ProcTempFileSave ();
                }
            }

            //input changed
            if ( message == 273 && wParam == /*327680*/50331648 )
            {
                if ( ( HWND )lParam == hwnd_serverurl )
                {
                    GetWindowText (hwnd_serverurl, serverURL, sizeof (serverURL));
                    config->WriteStringW (L"server_url", serverURL);
                }

                if ( ( HWND )lParam == hwnd_serverkey )
                {
                    GetWindowText (hwnd_serverkey, serverKEY, sizeof (serverKEY));
                    config->WriteStringW (L"server_key", serverKEY);
                }
            }
        }
        break;
    case WM_PAINT:
    {
        PAINTSTRUCT ps;
        HDC hdc = BeginPaint (hWnd, &ps);
        MoveToEx (hdc, 0, 120, 0);
        LineTo (hdc, 500, 120);
        MoveToEx (hdc, 0, 150, 0);
        LineTo (hdc, 500, 150);
        EndPaint (hWnd, &ps);
    }
    break;
    case WM_DESTROY:
        PostQuitMessage(0);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

