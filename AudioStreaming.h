#pragma once

#include "resource.h"

#define SAFE_DELE(p)    if(p) { delete (p); (p) = NULL; }
#define SAFE_FREE(p)    if(p) { free (p); (p) = NULL; }
#define SAFE_RELEASE(p) if(p) { (p)->Release(); (p) = NULL; }
#pragma comment(linker,"/manifestdependency:\"type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")
#pragma comment( lib, "d3d9.lib" )
#pragma comment( lib, "d3dx9.lib" )
#pragma comment( lib, "lame/release_msvc141_mt_release_sse2/libmp3lame-static.lib" )
#pragma comment( lib, "lame/release_msvc141_mt_release_sse2/libmpghip-static.lib" )
#pragma comment( lib, "Crypt32.lib" )
#pragma comment( lib, "advapi32.lib" )
#pragma comment( lib, "ws2_32.lib" )
#pragma comment( lib, "Shlwapi.lib" )
#pragma comment( lib, "libcurl/windows-Win32-v140/lib/libcurl_a.lib" )

#include "framework.h"
#include "commctrl.h"
#include "shlobj.h"
#include "Shlwapi.h"
#include <d3d9.h>
#include <d3dx9.h>
#include <stdio.h>
#include <map>
#include "fonts.h"
HINSTANCE				g_hinst = 0;
HWND					g_wnd = 0;
// Direct3D structures
BOOL					quit = FALSE;
HWND lbl_d3d;
DWORD VU2powerL = 0, VU2powerR = 0;
char lvu[10], rvu[10];
DWORD VUpowerL = 0;
DWORD VUpowerR = 0;
#include "cvumeter.h"
#include "cdbmeter.h"
CdBMeter *meter;
#include "flat_vu_png.h"
#include "cd3dgui.h"
#include "lame/lame.h"
#define CURL_STATICLIB
#include "libcurl\windows-Win32-v140\include\curl\curl.h"
#define MINIAUDIO_IMPLEMENTATION
#include "miniaudio.h"
#include <string>
#include <vector>
#include <deque>
HWND hwnd_deviceselect;
HWND hwnd_buttonstreaming = 0;
HWND hwnd_buttonsettings = 0;
HWND hwnd_serverurl = 0;
HWND hwnd_serverkey = 0;
HWND hwnd_mp3savepath = 0;
HWND hwnd_mp3pathselect = 0;
HWND hwnd_mp3savestatus = 0;
HWND hwnd_chlpowerdb = 0;
HWND hwnd_chrpowerdb = 0;
HWND lbl_networkstatus = 0;
DWORD isStreaming = FALSE;
DWORD defaultDevice = 0;
DWORD settingsPanel = TRUE;
DWORD saveMP3 = FALSE;
DWORD transmittedBytes = 0;
wchar_t mp3path[MAX_PATH];
wchar_t serverURL[200];
wchar_t serverKEY[60];
FILE *fd;
#include "util.h"
#include "winreg.h"
using namespace winreg;
#include "cpreferences.h"
CPreferences *config;
#define MAX_LOADSTRING 100
// Variables globales:
HINSTANCE hInst;                                // instancia actual
WCHAR szTitle[MAX_LOADSTRING];                  // Texto de la barra de t�tulo
WCHAR szWindowClass[MAX_LOADSTRING];            // nombre de clase de la ventana principal
void data_callback (ma_device * pDevice, void *pOutput, const void *pInput, ma_uint32 frameCount);
// Declaraciones de funciones adelantadas incluidas en este m�dulo de c�digo:
ATOM                MyRegisterClass (HINSTANCE hInstance);
BOOL                InitInstance (HINSTANCE, int);
LRESULT CALLBACK    WndProc (HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About (HWND, UINT, WPARAM, LPARAM);
std::deque<std::string> networkQueue;
#include "caudiocapture.h"
#include "caudio.h"
CAudio *audio;
#include "cmp3encoder.h"
CMp3Encoder *mp3;
#include "network.h"
char hash_save[30];
#include "pathmanager.h"
CD3DGUI *d3d_gui;
