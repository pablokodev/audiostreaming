#pragma once

class CAudio
{
public:
    CAudio ()
    {
        if ( ma_context_init (NULL, 0, NULL, &context) != MA_SUCCESS )
        {
            printf ("Failed to initialize context.\n");
            return;
        }
    }
    ~CAudio ()
    {
        SAFE_DELE (capture);
        ma_context_uninit (&context);
    }
    std::vector<std::pair<ma_device_type, ma_device_info *>>& QueryDevices ()
    {
        devices.clear ();

        if ( &context == NULL ) return devices;
        result = ma_context_get_devices (&context, &loopbackDeviceInfos, &loopbackDeviceCount, &captureDeviceInfos, &captureDeviceCount);
        if ( result != MA_SUCCESS )
        {
            printf ("Failed to retrieve device information.\n");
            return devices;
        }

        ma_uint32 iDevice;

        for ( iDevice = 0; iDevice < loopbackDeviceCount; ++iDevice )
        {
            devices.push_back (std::make_pair (ma_device_type_loopback, &loopbackDeviceInfos[iDevice]));
        }

        for ( iDevice = 0; iDevice < captureDeviceCount; ++iDevice )
        {
            devices.push_back (std::make_pair (ma_device_type_capture, &captureDeviceInfos[iDevice]));
        }

        return devices;
    }

    bool CaptureByID (int id)
    {
        try
        {
            auto dev = devices.at (id);
            if ( capture != nullptr ) SAFE_DELE (capture);
            capture = new CAudioCapture (&dev.second->id, dev.first, context);
            capture->Start ();
            return true;
        } catch (...)
        {
            return false;
        }
    }

    bool CaptureDefault (ma_device_type ty)
    {
        if ( capture != nullptr ) SAFE_DELE (capture);
        capture = new CAudioCapture (NULL, ty, context);
        return true;
    }

    CAudioCapture *GetCapture ()
    {
        return capture;
    }

private:
    std::vector<std::pair<ma_device_type, ma_device_info *>> devices;

    ma_result result;

    ma_context context;
    ma_device_info *loopbackDeviceInfos;
    ma_uint32 loopbackDeviceCount;
    ma_device_info *captureDeviceInfos;
    ma_uint32 captureDeviceCount;
    
    //char devname[MAX_PATH];
    CAudioCapture *capture = nullptr;
};

