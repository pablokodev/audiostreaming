#pragma once

class CAudioCapture
{
public:
    CAudioCapture (ma_device_id *id, ma_device_type captype, ma_context ctx) : deviceID (id), deviceType (captype), audioContext (ctx)
    {
        deviceConfig = ma_device_config_init (captype);
        deviceConfig.capture.format = ma_format::ma_format_s16;
        deviceConfig.capture.channels = 2;
        deviceConfig.sampleRate = 48000;
        deviceConfig.dataCallback = data_callback;
        deviceConfig.capture.pDeviceID = id;
        deviceConfig.periodSizeInMilliseconds = 10;
        deviceConfig.pUserData = this;
    }
    ~CAudioCapture ()
    {
        ma_device_uninit (&device);
    }
    bool Start ()
    {
        result = ma_device_init (&audioContext, &deviceConfig, &device);
        if ( result != MA_SUCCESS )
        {
            printf ("Failed to initialize capture device.\n");
            return false;
        }

        result = ma_device_start (&device);
        if ( result != MA_SUCCESS )
        {
            ma_device_uninit (&device);
            printf ("Failed to start device.\n");
            return false;
        }
        return true;
    }
    bool Stop ()
    {
        ma_device_stop (&device);
        return true;
    }
private:
    ma_device_id *deviceID;
    ma_device_type deviceType = ma_device_type_capture;
    ma_result result;
    ma_context audioContext;
    ma_device_config deviceConfig;
    ma_device device;
};

