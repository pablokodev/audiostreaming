#pragma once
class CD3DGUI
{
public:
	CD3DGUI (HWND hWnd)
	{
        win_dest = hWnd;
        bgColor = D3DCOLOR_ARGB (0, 25, 25, 25); // background color
        CreateD3D ();
        InitD3D ();
        InitVolatiles ();
	}
	
	~CD3DGUI ()
	{
        ReleaseVolatiles ();
        ReleaseD3D ();
	}

    void ReleaseD3D ()
    {
        SAFE_RELEASE (g_D3DDev);
        SAFE_RELEASE (g_D3D);
        SAFE_FREE (g_D3Dpp);
    }

    bool CreateD3D ()
    {
        g_D3D = Direct3DCreate9 (D3D_SDK_VERSION);
        if ( !g_D3D )
            return FALSE;
        g_D3Dpp = ( D3DPRESENT_PARAMETERS * )malloc (sizeof (D3DPRESENT_PARAMETERS));
        ZeroMemory (g_D3Dpp, sizeof (D3DPRESENT_PARAMETERS));
        g_D3Dpp->Windowed = TRUE;
        g_D3Dpp->BackBufferCount = 1;
        g_D3Dpp->SwapEffect = D3DSWAPEFFECT_FLIP;
        g_D3Dpp->BackBufferFormat = /*D3DFMT_A8R8G8B8*/D3DFMT_UNKNOWN;
        g_D3Dpp->hDeviceWindow = win_dest;
        g_D3Dpp->EnableAutoDepthStencil = TRUE;
        g_D3Dpp->AutoDepthStencilFormat = D3DFMT_D16;
        g_D3Dpp->PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;
        HRESULT hr = g_D3D->CreateDevice (D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, win_dest, D3DCREATE_SOFTWARE_VERTEXPROCESSING, g_D3Dpp, &g_D3DDev);
        if ( FAILED (hr) )
        {
            return FALSE;
        }
        return TRUE;
    }

    void Render ()
    {

        if ( CheckDeviceLost () ) return;

        UpdateVUFrame ();

        if ( g_D3DDev == NULL ) return;

        g_D3DDev->Clear (0, NULL, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, bgColor, 1.0f, 0);

        g_D3DDev->BeginScene ();
        
        if ( drawing )
        {
            drawing->Begin (D3DXSPRITE_ALPHABLEND);

            if ( pow_meter )
            {
                pow_meter->Render (70, 6, VUpowerL, 0xFFFFFFFF);
                pow_meter->Render (373, 6, VUpowerR, 0xFFFFFFFF);
            }

            if ( bg_tex )
                drawing->Draw (bg_tex, 0, 0, 0, 0xFFFFFFFF);
            
            if ( vu_meter )
            {
                vu_meter->Render (108, 25, VU2powerL, 0xFFFFFFFF);
                vu_meter->Render (243, 25, VU2powerR, 0xFFFFFFFF);
            }

            if ( db_font_render )
            {
                db_font_render->Draw (lvu, 26, 71, 0xFFAAAAAA);
                db_font_render->Draw (rvu, 408, 71, 0xFFAAAAAA);
            }

            drawing->End ();
        }
        g_D3DDev->EndScene ();
        g_D3DDev->Present (NULL, NULL, NULL, NULL);
    }

    void Release ()
    {
        delete this;
    }

private:
    void InitD3D ()
    {
        // Set viewport
        D3DVIEWPORT9 vp = { 0, 0, g_D3Dpp->BackBufferWidth, g_D3Dpp->BackBufferHeight, 0, 1 };
        g_D3DDev->SetViewport (&vp);
        // Set D3D matrices
        D3DMATRIX matId = { 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 };
        g_D3DDev->SetTransform (D3DTS_WORLD, &matId);
        g_D3DDev->SetTransform (D3DTS_VIEW, &matId);
        D3DMATRIX matProj = { ( float )g_D3Dpp->BackBufferHeight / g_D3Dpp->BackBufferWidth, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1 };
        g_D3DDev->SetTransform (D3DTS_PROJECTION, &matProj);
        // Disable lighting and culling
        g_D3DDev->SetRenderState (D3DRS_LIGHTING, FALSE);
        g_D3DDev->SetRenderState (D3DRS_CULLMODE, D3DCULL_NONE);
        g_D3DDev->SetRenderState (D3DRS_ZENABLE, D3DZB_FALSE);
    }

    void InitVolatiles ()
    {
        if ( !drawing )
            D3DXCreateSprite (g_D3DDev, &drawing);
        if ( !vu_meter )
            vu_meter = new CVUMeter (g_D3DDev, drawing, flat_vu_png, sizeof (flat_vu_png), 128, 8192, 95, 85, VERTICAL);
        if ( !bg_tex )
            D3DXCreateTextureFromFileInMemoryEx (g_D3DDev, bg_png, sizeof (bg_png), 480, 120, D3DX_DEFAULT, 0, D3DFMT_FROM_FILE, D3DPOOL_DEFAULT, D3DX_DEFAULT, D3DX_DEFAULT, 0, 0, 0, &bg_tex);
            //D3DXCreateTextureFromFileEx (g_D3DDev, L"audiostreamingbg.png", 480, 120, D3DX_DEFAULT, 0, D3DFMT_FROM_FILE, D3DPOOL_DEFAULT, D3DX_DEFAULT, D3DX_DEFAULT, 0, 0, 0, &bg_tex);
        if ( !pow_meter )
            pow_meter = new CVUMeter (g_D3DDev, drawing, vertical_vu_png, sizeof (vertical_vu_png), 35, 3720, 31, 120, VERTICAL);
        if ( !db_font_render )
            db_font_render = new CFontRender (&font_Consolas12, g_D3DDev, drawing);
    }

    void ReleaseVolatiles ()
    {
        SAFE_RELEASE (vu_meter);
        SAFE_RELEASE (pow_meter);
        SAFE_RELEASE (bg_tex);
        SAFE_RELEASE (db_font_render);
        SAFE_RELEASE (drawing);
    }

    BOOL CheckDeviceLost ()
    {
        HRESULT hr = D3DERR_DEVICELOST;

        if ( g_D3DDev != NULL )
            hr = g_D3DDev->TestCooperativeLevel ();

        if ( hr != D3D_OK )
        {
            ReleaseVolatiles ();
            Sleep (100);
            ReleaseD3D ();
            Sleep (100);
            if ( CreateD3D () )
            {
                InitD3D ();
                InitVolatiles ();
            }
            else return TRUE;
        }
        return FALSE;
    }
    void UpdateVUFrame ()
    {
        VUpowerL = meter->CalcChanneldB (0, &aa);
        VUpowerR = meter->CalcChanneldB (1, &bb);

        if ( aa > 0.0f ) aa = sqrt (aa);
        if ( bb > 0.0f ) bb = sqrt (bb);

        sprintf (lvu, "%06.2f", aa);
        sprintf (rvu, "%06.2f", bb);

        if ( lvu[0] == '0' ) lvu[0] = '+';
        if ( rvu[0] == '0' ) rvu[0] = '+';

        if ( VU2powerL <= VUpowerL )
            VU2powerL = VUpowerL;
        else
            if ( ( VU2powerL - VUpowerL ) / 6 > 0 )
                VU2powerL -= ( VU2powerL - VUpowerL ) / 6;
            else
                VU2powerL--;
        if ( VU2powerL < 0 ) VU2powerL = 0;

        if ( VU2powerR <= VUpowerR )
            VU2powerR = VUpowerR;
        else
            if ( ( VU2powerR - VUpowerR ) / 6 > 0 )
                VU2powerR -= ( VU2powerR - VUpowerR ) / 6;
            else
                VU2powerR--;
        if ( VU2powerR < 0 ) VU2powerR = 0;
    }
private:
    HWND win_dest;
	IDirect3D9 *g_D3D = NULL;
	IDirect3DDevice9 *g_D3DDev = NULL;
	D3DPRESENT_PARAMETERS*   g_D3Dpp;
    CVUMeter *vu_meter = 0;
    CVUMeter *pow_meter = 0;
    IDirect3DTexture9 *bg_tex = 0;
    ID3DXSprite *drawing = 0;
    CFontRender *db_font_render = 0;
    unsigned int bgColor = 0;
    RECT src_rect;
    D3DXVECTOR3 center_vec, position_vec;
    float aa, bb;
};