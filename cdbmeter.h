#pragma once

class CdBMeter
{
public:
    CdBMeter (int channels) : number_channels (channels)
    {
        vumin = new float[channels];
        vumax = new float[channels];
        //vusum = new float[channels];
        //vucnt = new int[channels];
    }
    ~CdBMeter ()
    {
        SAFE_FREE (vumin);
        SAFE_FREE (vumax);
        //SAFE_FREE (vusum);
        //SAFE_FREE (vucnt);
    }
    void AppendSamples (const short *samples, int count)
    {
        for ( int i = 0; i < ( count * number_channels ); i += number_channels )
        {
            for ( int c = 0; c < number_channels; c++ )
            {
                float sample = ( ( float )samples[i + c] ) / ( float )32768;
                
                vumin[c] = ( sample < vumin[c] ? sample : vumin[c] );
                vumax[c] = ( sample > vumax[c] ? sample : vumax[c] );
            }
        }
    }
    float CalcChanneldB (unsigned int ch, float* pwr)
    {
        if ( ch >= number_channels ) return NULL;
        
        float power = log10 (max (vumax[ch], -vumin[ch])) * 20.0f;
        if ( pwr ) pwr[0] = power + 8.0f;
        if ( power < -20.0f ) power = -20.0f;
        power += 20.0f; power = ( power * 100.0f ) / 20.0f;
        vumax[ch] = 0, vumin[ch] = 0;

        /*float power = log10 (sqrt (vusum[ch] / vucnt[ch])) * 20.0f;
        //power += 6.0f;
        if ( pwr ) pwr[0] = power;
        if ( power < -20.0f ) power = -20.0f;
        power += 20.0f; power = ( power * 100.0f ) / 20.0f;
        vusum[ch] = 0, vucnt[ch] = 0;*/

        return power;
    }
private:
    int number_channels;
    float *vumin;
    float *vumax;
    /*float *vusum;
    int *vucnt;*/
};


