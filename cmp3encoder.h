#pragma once

class CMp3Encoder
{
public:
    CMp3Encoder ()
    {
        mp3encoder = lame_init ();
        lame_set_in_samplerate (mp3encoder, 48000);
        lame_set_VBR (mp3encoder, vbr_default);
        lame_set_bWriteVbrTag (mp3encoder, 0);
        lame_set_num_channels (mp3encoder, 2);
        lame_set_VBR_mean_bitrate_kbps (mp3encoder, 128);
        lame_set_VBR_max_bitrate_kbps (mp3encoder, 132);
        lame_init_params (mp3encoder);
    }
    ~CMp3Encoder ()
    {
        while ( lame_encode_flush (mp3encoder, mp3buffer, sizeof (mp3buffer)) != 0 ) {}
        lame_close (mp3encoder);
    }
    int Encode (const short pcm[], const int nsamples, unsigned char *buffer, size_t size)
    {
        int ret = lame_encode_buffer_interleaved (mp3encoder, ( short * )pcm, nsamples, buffer, size /*mp3buffer, sizeof (mp3buffer)*/);
        if ( file != NULL && ret > 0 )
            fwrite (/*mp3buffer*/buffer, 1, ret, file);
        return ret;
    }
    /*unsigned char *GetBuffer ()
    {
        return mp3buffer;
    }*/
    void SetFile (FILE *fd)
    {
        file = fd;
    }
private:
    unsigned char mp3buffer[500 + 7200];
    lame_t mp3encoder;
    FILE *file = NULL;
};

