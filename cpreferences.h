#pragma once

class CPreferences
{
public:
    CPreferences ()
    {
        regKey = new RegKey (HKEY_CURRENT_USER, L"SOFTWARE\\AudioStreaming");
    }

    ~CPreferences ()
    {
        regKey->Close ();
        SAFE_DELE (regKey);
    }

    bool ReadDWORD (const wchar_t *key, DWORD* dw)
    {
        try {
            *dw = regKey->GetDwordValue (key);
            return true; 
        } catch ( RegException ex ) { }
        return false;
    }

    bool ReadStringW (const wchar_t *key, wchar_t *ret)
    {
        try {
            if ( regKey->QueryValueType (key) )
            {
                wsprintf (ret, L"%s", regKey->GetStringValue (key).c_str ());
                return true; //( wchar_t * )regKey->GetStringValue (key).c_str ();
            }
        } catch ( RegException ex ) { }
        return false;
    }

    void WriteDWORD (const wchar_t *key, DWORD value)
    {
        regKey->SetDwordValue (key, value);
    }

    void WriteStringW (const wchar_t *key, const wchar_t *value)
    {
        regKey->SetStringValue (key, value);
    }

    void WriteStringA (const wchar_t *key, const char *value)
    {
        wchar_t *buf = _convert_UTF8_to_wchar (value);
        WriteStringW (key, buf);
        SAFE_FREE (buf);
    }
private:
    RegKey *regKey;
};

