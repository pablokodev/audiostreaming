#pragma once

enum sprite_dir
{
	VERTICAL,
	HORIZONTAL
};

class CVUMeter
{
public:
	CVUMeter (IDirect3DDevice9 *device, ID3DXSprite *sprite, unsigned char *tex_addr, size_t tex_size, int width, int height, int sprite_count, int sprite_size, sprite_dir sprite_dir)
	{
		if ( sprite == NULL )
			D3DXCreateSprite (device, &spr), manage_sprite = true;
		else
			spr = sprite, manage_sprite = false;
		count = sprite_count;
		size = sprite_size;
		dir = sprite_dir;
		w = width;
		h = height;
		D3DXCreateTextureFromFileInMemoryEx (device, tex_addr, tex_size, width, height, D3DX_DEFAULT, 0, D3DFMT_FROM_FILE, D3DPOOL_DEFAULT, D3DX_DEFAULT, D3DX_DEFAULT, 0, 0, 0, &tex);
	}

	~CVUMeter ()
	{
		SAFE_RELEASE (tex);
		if ( manage_sprite )
			SAFE_RELEASE (spr);
	}

	void Release ()
	{
		delete this;
	}

	void Render (int x, int y, unsigned int pos, D3DCOLOR color)
	{
		if ( pos > 100 ) pos = 100;
		if ( manage_sprite )
			spr->Begin (D3DXSPRITE_ALPHABLEND);
		RECT src_rect;
		if ( dir == VERTICAL )
			src_rect.top = size * ( pos * count / 100 ), src_rect.left = 0, src_rect.right = w, src_rect.bottom = size + ( size * ( pos * count / 100 ) );
		if ( dir == HORIZONTAL )
			src_rect.left = size * ( pos * count / 100 ), src_rect.top = 0, src_rect.bottom = h, src_rect.right = size + ( size * ( pos * count / 100 ) );
		center_vec.x = -x; center_vec.y = -y; center_vec.z = 1; position_vec.x = 0; position_vec.y = 0; position_vec.z = 1;
		spr->Draw (tex, &src_rect, &center_vec, &position_vec, color);
		if ( manage_sprite )
			spr->End ();
	}
private:
	IDirect3DTexture9 *tex;
	ID3DXSprite *spr;
	D3DXVECTOR3 center_vec, position_vec;
	bool manage_sprite;
	int count, size, w, h;
	sprite_dir dir;
};

