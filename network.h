#pragma once

size_t net_read_callback (char *buffer, size_t size, size_t nitems, void *opaque)
{
    if ( !isStreaming )
        return -1;

    size_t sRet = 0;
    while ( sRet == 0 )
    {

        while ( networkQueue.size () == 0 )
        {
            if ( !isStreaming )
                return -1;
            Sleep (1);
        }

        std::string &pkt = networkQueue.front ();

        if ( mp3 )
        {
            int mp3Len = mp3->Encode (( short * )pkt.c_str (), pkt.size () / 4, (unsigned char*)buffer, size*nitems);
            if ( mp3Len > 0 )
            {
                if ( isStreaming && networkQueue.size () < 100 )
                {
                    //unsigned char *buf = mp3->GetBuffer ();
                    //memcpy (buffer, buf, mp3Len);
                    sRet = mp3Len;
                }
            }
        }

        networkQueue.pop_front ();
        transmittedBytes += sRet;
    }
    return sRet;
}

DWORD WINAPI NetworkProcessor (void *)
{
    Sleep (500); //Wait til headers are dumped
    //Init curl stuff, winsock, etc
    CURLcode pResponse;
    pResponse = curl_global_init (CURL_GLOBAL_DEFAULT);
    if ( pResponse != CURLE_OK )
    {
        printf ("curl_global_init() failed: %s\n", curl_easy_strerror (pResponse));
        return 1;
    }

    for (;;)
    {
        while ( !isStreaming )
            Sleep (100);

        SetWindowText (lbl_networkstatus, L"Connecting...");
        char *server_url = _convert_wchar_to_UTF8 (serverURL);
        char *server_key = _convert_wchar_to_UTF8 (serverKEY);
        CURL *pCurl;
        pCurl = curl_easy_init ();
        if ( pCurl == NULL )
            return E_UNEXPECTED;

        curl_easy_setopt (pCurl, CURLOPT_URL, server_url);
        curl_easy_setopt (pCurl, CURLOPT_PUT, 1L);
        curl_easy_setopt (pCurl, CURLOPT_READFUNCTION, net_read_callback);
        curl_easy_setopt (pCurl, CURLOPT_READDATA, &pCurl);
        curl_easy_setopt (pCurl, CURLOPT_VERBOSE, 1L);
        curl_easy_setopt (pCurl, CURLOPT_UPLOAD, 1L);
        curl_easy_setopt (pCurl, CURLOPT_CONNECTTIMEOUT, 5);

        struct curl_slist *pHeaderList = NULL;

        char szAuthHeader[255];
        sprintf (szAuthHeader, "Authorization: %s", server_key);
        pHeaderList = curl_slist_append (pHeaderList, szAuthHeader);

        pHeaderList = curl_slist_append (pHeaderList, "Transfer-Encoding: chunked");
        pHeaderList = curl_slist_append (pHeaderList, "Expect:");
        pResponse = curl_easy_setopt (pCurl, CURLOPT_HTTPHEADER, pHeaderList);

        networkQueue.clear ();

        pResponse = curl_easy_perform (pCurl); // blocking task
        if ( pResponse != CURLE_OK )
            printf ("curl_easy_perform() failed: %s\n", curl_easy_strerror (pResponse));
        else
            printf ("curl_easy_perform() ended gracefully");

        SetWindowText (lbl_networkstatus, L"Disconnected");
        curl_easy_cleanup (pCurl);
        SAFE_FREE (server_url);
        SAFE_FREE (server_key);
        Sleep (500); //Dont reconnect too fast
    }
    //Deinit todo: move to dtors
    curl_global_cleanup ();
    return S_OK;
}

