#pragma once

DWORD __stdcall PathSelect (void *p)
{
    if ( GetFolder (mp3path, L"Pick a folder to store MP3 files", 0) )
    {
        SetWindowText (hwnd_mp3savepath, mp3path);
        config->WriteStringW (L"mp3_save_path", mp3path);
    }
    return 0;
}

void ProcTempFileSave ()
{
    if ( !saveMP3 )
    {
        if ( fd != NULL )
        {
            sprintf (hash_save, "");
            if ( mp3 )
                mp3->SetFile (NULL);
            fclose (fd);
            fd = NULL;
        }
        return;
    }
    SYSTEMTIME time;
    GetLocalTime (&time);
    static char filename[MAX_PATH];
    static char hash[30];
    sprintf (hash, "%d_%d_%d_%d", time.wYear, time.wMonth, time.wDay, time.wHour);

    if ( strcmp (hash, hash_save) != 0 )
    {
        sprintf (hash_save, "%s", hash);
        char *mp3pathutf8 = _convert_wchar_to_UTF8 (mp3path);
        sprintf (filename, "%s\\AudioStreaming_rec_%d_%d_%d_%d_%d_%d.mp3", mp3pathutf8,
                 time.wYear, time.wMonth, time.wDay, time.wHour, time.wMinute, time.wSecond);
        SAFE_FREE (mp3pathutf8);

        if ( fd != NULL )
        {
            mp3->SetFile (NULL);
            fclose (fd);
        }
        fd = fopen (filename, "wb");
        mp3->SetFile (fd);
    }
}

