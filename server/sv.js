////////////////////////////////////////////////////////////
//                 AUDIO STREAMING SERVER                 //
////////////////////////////////////////////////////////////
var port = 8080; //Listening http port
var server_key = 'SecretChangeMe'; //Server key, stream keys are md5(name+stream_key)
////////////////////////////////////////////////////////////
var md5 = require('md5');
//var fs = require('fs');
var events = require('events');
var Emitters = {}
var Streams = {};
//var player_html = fs.readFileSync( 'radio.html' ).toString();
////////////////////////////////////////////////////////////
var GetEmitter = function(feed)
{
    if(!Emitters[feed])
        Emitters[feed] = new events.EventEmitter().setMaxListeners(0)
    return Emitters[feed]
}
////////////////////////////////////////////////////////////
var fix_stream_name = function(n)
{
	if (n != n.replace(/[^a-zA-Z ]/g, "")) return null;
	if (n.length > 30) return null;
	if (n.length < 2) return null;
	return n;
}
////////////////////////////////////////////////////////////
var validate_stream_key = function(n, key)
{
	var ckey = md5(n + server_key); 
	console.log('stream',n,key,ckey);
	if (key != ckey) return false;
	return true;
}
////////////////////////////////////////////////////////////
var validate_stream_exists = function(n, create)
{
	if (Streams[n]) return false;
	if (create) Streams[n]={ buffer:[] };
	return true;
}
////////////////////////////////////////////////////////////
require('http').createServer((req, res)=>{
	
	//URL parser
	var urlcomp = req.url.toLowerCase().split('/');
	urlcomp.shift(); //Remove fisrt empty string
	if (urlcomp[urlcomp.length - 1] == '') urlcomp.pop(); //Remove last position if empty

	//URL processor

	//Stream upload
	if (req.method == "PUT" && urlcomp.length == 3 && urlcomp[0]=="liveradio" && urlcomp[1]=="stream")
	{
		//Check stream name
		var stream_name = fix_stream_name(urlcomp[2]);
		if (!stream_name) return req.destroy(),res.end('');
		//Check stream key
		if (!validate_stream_key(stream_name, req.headers.authorization)) return req.destroy(),res.end('');
		//Check stream is already online and create resources
		if (!validate_stream_exists(stream_name, true)) return req.destroy(),res.end('');

		//Request events handling
		req.on('close', ()=>{
			delete Streams[stream_name]; //free resources, timeout exit
		}).on('end', ()=>{
			delete Streams[stream_name]; //free resources, graceful exit
		}).on('data', (d)=>{
			//MP3 Cache store, remove old cache
			Streams[stream_name].buffer.push(d)
			while (Streams[stream_name].buffer.length > 326) 
				Streams[stream_name].buffer.shift();
			//Redirect MP3 data to live listeners
			GetEmitter(stream_name).emit('data', d);
		})

	}

	//Stream listen
	else if (req.method == "GET" && urlcomp.length == 3 && urlcomp[0]=="liveradio" && urlcomp[2]=="live.mp3")
	{
		//Check stream name and streaming online status
		var stream_name = fix_stream_name(urlcomp[1]);
		if (!stream_name) return req.destroy(),res.end('');
		if (validate_stream_exists(stream_name, false)) return req.destroy(),res.end('');

		//Append http headers
		res.setHeader('Content-Type', 'audio/mpeg');
    	res.setHeader('Access-Control-Allow-Origin','*');

    	//Dump all the MP3 cache for this stream, this will buffer faster
    	res.write(Buffer.concat(Streams[stream_name].buffer));

    	//Suscribe to stream emitter and dump its frames.
    	var cb = null; var emt = GetEmitter(stream_name).on('data', cb = function(d) {
			res.write(d);
		})
		//Free resources when client exit
		res.on('close', function () {
	        emt.removeListener('data',cb)
	    })
	}

	//Todo: Player not needed now...
	/*else if (req.method == "GET" && urlcomp.length == 2 && urlcomp[0]=="liveradio")
	{
		var stream_name = fix_stream_name(urlcomp[1]);
		if (!stream_name) return res.end('');
		if (validate_stream_exists(stream_name, false)) return res.end('');

		res.setHeader('Content-Type', 'text/html');
		res.end(player_html.replace('[[TITLE]]', stream_name));
	}*/

	//Default to silence
	else {
		return res.end('');
	}
	
}).listen(port,"0.0.0.0")
////////////////////////////////////////////////////////////
