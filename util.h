#pragma once

/**
 * @brief 
 * @param str_utf8 
 * @return 
*/
wchar_t *_convert_UTF8_to_wchar (const char *str_utf8)
{
    wchar_t *str_w = NULL;
    if ( str_utf8 )
    {
        int str_w_len = MultiByteToWideChar (CP_UTF8, MB_ERR_INVALID_CHARS,
                                             str_utf8, -1, NULL, 0);
        if ( str_w_len > 0 )
        {
            str_w = ( wchar_t * )malloc (str_w_len * sizeof (wchar_t));
            if ( str_w )
            {
                if ( MultiByteToWideChar (CP_UTF8, 0, str_utf8, -1, str_w,
                                          str_w_len) == 0 )
                {
                    free (str_w);
                    return NULL;
                }
            }
        }
    }
    return str_w;
}

/**
 * @brief 
 * @param str_w 
 * @return 
*/
char *_convert_wchar_to_UTF8 (const wchar_t *str_w)
{
    char *str_utf8 = NULL;
    if ( str_w )
    {
        int str_utf8_len = WideCharToMultiByte (CP_UTF8, 0, str_w, -1, NULL,
                                                0, NULL, NULL);
        if ( str_utf8_len > 0 )
        {
            str_utf8 = ( char * )malloc (str_utf8_len * sizeof (wchar_t));
            if ( str_utf8 )
            {
                if ( WideCharToMultiByte (CP_UTF8, 0, str_w, -1, str_utf8, str_utf8_len,
                                          NULL, FALSE) == 0 )
                {
                    free (str_utf8);
                    return NULL;
                }
            }
        }
    }
    return str_utf8;
}

bool GetFolder (wchar_t *folderpath,
                const wchar_t *szCaption = NULL,
                HWND hOwner = NULL)
{
    bool retVal = false;

    // The BROWSEINFO struct tells the shell 
    // how it should display the dialog.
    BROWSEINFO bi;
    memset (&bi, 0, sizeof (bi));

    bi.ulFlags = BIF_USENEWUI;
    bi.hwndOwner = hOwner;
    bi.lpszTitle = szCaption;

    // must call this if using BIF_USENEWUI
    ::OleInitialize (NULL);

    // Show the dialog and get the itemIDList for the 
    // selected folder.
    LPITEMIDLIST pIDL = ::SHBrowseForFolder (&bi);

    if ( pIDL != NULL )
    {
       // Create a buffer to store the path, then 
       // get the path.
        //wchar_t buffer[_MAX_PATH] = { '\0' };
        if ( ::SHGetPathFromIDList (pIDL, folderpath) != 0 )
        {
           // Set the string value.
            //folderpath = buffer;
            retVal = true;
        }

        // free the item id list
        CoTaskMemFree (pIDL);
    }

    ::OleUninitialize ();

    return retVal;
}

void fillDeviceListBox (std::vector<std::pair<ma_device_type, ma_device_info *>> &devices, int selected)
{
    char devname[MAX_PATH];
    SendMessage (hwnd_deviceselect, CB_RESETCONTENT, ( WPARAM )NULL, ( LPARAM )NULL);

    for ( auto const &dev : devices )
    {
        if ( dev.first != ma_device_type_capture)
            sprintf (devname, "[OUTPUT]    %s", dev.second->name);
        else
            sprintf (devname, "[CAPTURE]  %s", dev.second->name);
        wchar_t *szdev = _convert_UTF8_to_wchar (devname);
        SendMessage (hwnd_deviceselect, ( UINT )CB_ADDSTRING, ( WPARAM )NULL, ( LPARAM )szdev);
        SAFE_FREE (szdev);
    }

    SendMessage (hwnd_deviceselect, CB_SETCURSEL, ( WPARAM )selected, ( LPARAM )NULL);
}

void CreateWinApiGUI (HWND hWnd, HINSTANCE hInstance)
{
    //pwer meters
   //hwnd_chlpowerdb = CreateWindow (PROGRESS_CLASS, L"", PBS_VERTICAL | PBS_SMOOTH | WS_CHILD | WS_OVERLAPPED | WS_VISIBLE, 20, 20, 70, 80, hWnd, NULL, hInstance, NULL);
   //hwnd_chrpowerdb = CreateWindow (PROGRESS_CLASS, L"", PBS_VERTICAL | PBS_SMOOTH | WS_CHILD | WS_OVERLAPPED | WS_VISIBLE , 390, 20, 70, 80, hWnd, NULL, hInstance, NULL);
   //select device
    hwnd_deviceselect = CreateWindow (WC_COMBOBOX, L"", CBS_DROPDOWNLIST | CBS_HASSTRINGS | WS_BORDER | WS_VSCROLL | WS_CHILD | WS_OVERLAPPED | WS_VISIBLE | CBS_AUTOHSCROLL, 10, 170, 460, 220, hWnd, NULL, hInstance, NULL);

    //server url and key
    hwnd_serverurl = CreateWindow (WC_EDIT, L"", WS_BORDER | WS_CHILD | WS_OVERLAPPED | WS_VISIBLE | ES_AUTOHSCROLL | ES_MULTILINE, 10, 210, 260, 20, hWnd, NULL, hInstance, NULL);
    hwnd_serverkey = CreateWindow (WC_EDIT, L"", WS_BORDER | WS_CHILD | WS_OVERLAPPED | WS_VISIBLE | ES_AUTOHSCROLL | ES_MULTILINE, 280, 210, 190, 20, hWnd, NULL, hInstance, NULL);

    //mp3 save opts
    hwnd_mp3savepath = CreateWindow (WC_EDIT, L"", CBS_SIMPLE | WS_BORDER | WS_CHILD | WS_OVERLAPPED | WS_VISIBLE | WS_DISABLED | ES_AUTOHSCROLL | ES_MULTILINE, 120, 250, 290, 20, hWnd, NULL, hInstance, NULL);
    hwnd_mp3pathselect = CreateWindow (WC_BUTTON, L"...", WS_CHILD | WS_OVERLAPPED | WS_VISIBLE, 419, 250, 50, 22, hWnd, NULL, hInstance, NULL);
    hwnd_mp3savestatus = CreateWindow (WC_BUTTON, L"NOT RECORDING", WS_CHILD | WS_OVERLAPPED | WS_VISIBLE, 10, 250, 100, 22, hWnd, NULL, hInstance, NULL);

    //buttons
    hwnd_buttonstreaming = CreateWindow (WC_BUTTON, isStreaming ? L"STREAMING" : L"NOT STREAMING", BS_PUSHBUTTON | WS_CHILD | WS_OVERLAPPED | WS_VISIBLE, 280, 125, 100, 21, hWnd, NULL, hInstance, NULL);
    hwnd_buttonsettings = CreateWindow (WC_BUTTON, L"SETTINGS", BS_PUSHBUTTON | WS_CHILD | WS_OVERLAPPED | WS_VISIBLE, 390, 125, 80, 21, hWnd, NULL, hInstance, NULL);

    //labels
    HWND lbl_device = CreateWindow (WC_STATIC, L"Audio Device", WS_CHILD | WS_OVERLAPPED | WS_VISIBLE, 10, 155, 260, 14, hWnd, NULL, hInstance, NULL);
    HWND lbl_serverurl = CreateWindow (WC_STATIC, L"Stream URL", WS_CHILD | WS_OVERLAPPED | WS_VISIBLE, 10, 195, 260, 14, hWnd, NULL, hInstance, NULL);
    HWND lbl_serverkey = CreateWindow (WC_STATIC, L"Stream Key", WS_CHILD | WS_OVERLAPPED | WS_VISIBLE, 280, 195, 260, 14, hWnd, NULL, hInstance, NULL);
    HWND lbl_mp3recording = CreateWindow (WC_STATIC, L"MP3 Recording", WS_CHILD | WS_OVERLAPPED | WS_VISIBLE, 10, 235, 260, 14, hWnd, NULL, hInstance, NULL);

    lbl_networkstatus = CreateWindow (WC_STATIC, L"Disconnected", WS_CHILD | WS_OVERLAPPED | WS_VISIBLE, 10, 130, 260, 14, hWnd, NULL, hInstance, NULL);
    //HWND lbl_heading = CreateWindow (WC_STATIC, L"AudioStreaming 1.1 - euWebHost.com", WS_CHILD | WS_OVERLAPPED | WS_VISIBLE, 10, 10, 460, 24, hWnd, NULL, hInstance, NULL);
    lbl_d3d = CreateWindow (WC_STATIC, L"", WS_CHILD | WS_OVERLAPPED | WS_VISIBLE, 0, 0, 480, 120, hWnd, NULL, hInstance, NULL);

    //styles
    HFONT hfont_normal = CreateFont (14, 0, 0, 0, FW_DONTCARE, FALSE, FALSE, FALSE, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, L"Arial");
    HFONT hfont_title = CreateFont (12, 0, 0, 0, FW_BOLD, FALSE, FALSE, FALSE, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, L"Arial");
    HFONT hfont_heading = CreateFont (16, 0, 0, 0, FW_BOLD, FALSE, FALSE, FALSE, ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_SWISS, L"Arial");

    SendMessage (hwnd_deviceselect, WM_SETFONT, WPARAM (hfont_normal), TRUE);

    SendMessage (hwnd_serverurl, WM_SETFONT, WPARAM (hfont_normal), TRUE);
    SendMessage (hwnd_serverkey, WM_SETFONT, WPARAM (hfont_normal), TRUE);
    SendMessage (hwnd_mp3savepath, WM_SETFONT, WPARAM (hfont_normal), TRUE);
    SendMessage (hwnd_mp3pathselect, WM_SETFONT, WPARAM (hfont_normal), TRUE);
    SendMessage (hwnd_mp3savestatus, WM_SETFONT, WPARAM (hfont_normal), TRUE);
    SendMessage (hwnd_buttonsettings, WM_SETFONT, WPARAM (hfont_normal), TRUE);
    SendMessage (hwnd_buttonstreaming, WM_SETFONT, WPARAM (hfont_normal), TRUE);

    SendMessage (lbl_device, WM_SETFONT, WPARAM (hfont_title), TRUE);
    SendMessage (lbl_serverurl, WM_SETFONT, WPARAM (hfont_title), TRUE);
    SendMessage (lbl_serverkey, WM_SETFONT, WPARAM (hfont_title), TRUE);
    SendMessage (lbl_mp3recording, WM_SETFONT, WPARAM (hfont_title), TRUE);

    SendMessage (lbl_networkstatus, WM_SETFONT, WPARAM (hfont_normal), TRUE);
    //SendMessage (lbl_heading, WM_SETFONT, WPARAM (hfont_heading), TRUE);

    //set default path
    SendMessage (hwnd_mp3savepath, ( UINT )WM_SETTEXT, ( WPARAM )NULL, ( LPARAM )mp3path);

    //set default state
    if ( saveMP3 )
    {
        SetWindowText (hwnd_mp3savestatus, L"RECORDING");
        EnableWindow (hwnd_mp3pathselect, FALSE);
    }

    //set default prefs
    SetWindowText (hwnd_serverurl, serverURL);
    SetWindowText (hwnd_serverkey, serverKEY);

    //timer for vumeter
    //SetTimer (hWnd, 0, 30, 0);
    //timer for file proc
    SetTimer (hWnd, 1, 1000, 0);
}

